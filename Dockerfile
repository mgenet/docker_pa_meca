################################################################################
###                                                                          ###
### Created by Martin Genet for MEC552B                                      ###
###                                                                          ###
### École Polytechnique, Palaiseau, France                                   ###
###                                                                          ###
################################################################################

# basic stuff
FROM continuumio/miniconda3
RUN apt-get update; apt-get -y install git rename libgl1-mesa-dev libglu1-mesa-dev libxcursor-dev libxft-dev libxinerama1 gcc-multilib xvfb; apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# computational stuff
RUN conda install -c conda-forge fenics=2019.1.0 ipywidgets=7.6.4 itkwidgets=0.32.0 jupyter=1.0.0 jupyter_contrib_nbextensions=0.5.1 jupyter_nbextensions_configurator=0.4.1 matplotlib=3.4.3 meshio=5.0.0 notebook=6.4.3 numpy=1.21.2 pip python=3.7.10 scipy=1.7.1 sympy=1.8 vtk=9.0.3 vtkplotter=2020.3.1
RUN pip install zstandard==0.16.0 gmsh==4.8.4 pygmsh==6.1.1 myPythonLibrary myVTKPythonLibrary dolfin_mech dolfin_warp

# jupyter stuff
RUN jupyter nbextension enable highlight_selected_word/main
RUN jupyter nbextension enable collapsible_headings/main
RUN jupyter nbextension enable toc2/main
RUN jupyter nbextension enable load_tex_macros/main
RUN jupyter nbextension enable varInspector/main
RUN jupyter nbextension enable spellchecker/main
RUN jupyter nbextension enable freeze/main
RUN jupyter nbextension enable livemdpreview/livemdpreview
RUN jupyter nbextension enable scratchpad/main
RUN jupyter nbextension enable python-markdown/main
